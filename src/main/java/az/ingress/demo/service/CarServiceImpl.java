package az.ingress.demo.service;

import az.ingress.demo.model.Car;
import az.ingress.demo.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository){

        this.carRepository = carRepository;
    }

    @Override
   public Car get(Integer id) {
        log.info("Car service get method is working");
        Optional<Car>car = carRepository.findById(id);
        if(car.isEmpty()){
            throw new RuntimeException("Car is not found");
        }

    return car.get();
}

    @Override
    public Car create(Car car){
        log.info("Car service create method is working");
        Car carInDb = carRepository.save(car);
        return carInDb;

    }
    @Override
    public Car update(Integer id, Car car){
        log.info("Car service update method is working");
        Car entity= carRepository.findById(id)
                .orElseThrow(()-> new RuntimeException("Car is not found"));
        entity.setId(car.getId());
        entity.setModel(car.getModel());
        entity.setModelYear(car.getModelYear());
        entity.setPrice(car.getPrice());
        entity.setColor(car.getColor());
        entity.setCondition(car.isCondition());
      entity = carRepository.save(entity);
      return entity;
    }

    @Override
    public void delete(Integer id){
        log.info("Car service delete method is working");
        carRepository.deleteById(id);

    }

}
