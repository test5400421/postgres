package az.ingress.demo.service;

import az.ingress.demo.model.Car;

public interface CarService {
    Car get(Integer id);

    Car create(Car car);

   Car update(Integer id,Car car);



    void delete(Integer id);

}
