package az.ingress.demo.controller;

import az.ingress.demo.model.Car;
import az.ingress.demo.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")

public class CarController {

    private final CarService carService;


    public CarController(CarService carService) {

        this.carService = carService;
    }

    @GetMapping("/{id}")
    public Car get(@PathVariable Integer id){

        return carService.get(id);
    }


    @PostMapping()
    public Car create(@RequestBody Car car){
        return carService.create(car);
    }
    @PutMapping("/{id}")
    public Car update(@PathVariable Integer id,@RequestBody Car car){

        return carService.update(id,car);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        carService.delete(id);

    }

}
